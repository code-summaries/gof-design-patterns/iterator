<div align="center">
  <h1>Iterator</h1>
</div>

<div align="center">
  <img src="iterator_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Iterator is a behavioral pattern to traverse elements of a collection without exposing its
underlying representation.**

### Real-World Analogy

_A record player._

The listener (client) can hear songs on the record (collection) sequentially using the record player (iterator) without
knowing how the songs are stored.

### Participants

- :bust_in_silhouette: **Interface**
    - Provides an interface to:
        - `method` object (aka `other_method`)
    - Optionally provides an interface to:
        - `method` object

- :man: **ConcreteObject**
    - ...

• AbstractExpression (RegularExpression)- declares an abstract Interpret operation that is common to all nodes in the
abstract syntax tree.246 BEHAVIORAL PATTERNS CHAPTER 5 • TerminalExpression (LiteralExpression)-implements an Interpret
operation associated with terminal symbols in the grammar.-an instance is required for every terminal symbol in a
sentence. • NonterminalExpression (AlternationExpression, RepetitionExpression, SequenceExpressions)- one such class is
required for every rule R ::= R\R^ • •. Rn in the grammar.- maintains instance variables of type AbstractExpression for
each of the symbols RI through Rn.-implements an Interpret operation for nonterminal symbols in the grammar. Interpret
typically calls itself recursively on the variables representing RI through Jin. • Context- contains information that's
global to the interpreter. • Client-builds (or is given) an abstract syntax tree representing a particular sentence in
the language that the grammar defines. The abstract syntax tree is assembled from instances of the NonterminalExpression
and TerminalExpression classes.-invokes the Interpret operation

### Collaborations

...
A Concretelterator keeps track of the current object in the aggregate and can
compute the succeeding object in the traversal.

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When ... .**

Use the Iterator pattern
• to access an aggregate object's contents without exposing its internal representation.
• to support multiple traversals of aggregateobjects.
• to provide a uniform interface for traversing different aggregate structures
(that is, to support polymorphic iteration)

### Motivation

- ...

An aggregate object such as a list should give you a way to access its elements
without exposing itsinternal structure. Moreover, you might want to traverse the
list in different ways, depending on what you want to accomplish. But you probably don't want to bloat the List
interface with operations for different traversals,
even if you could anticipate the ones you will need. Youmight also need to have
more than one traversal pending on the same list.

The Iterator pattern lets you do all this. The key idea in this pattern is to take the
responsibility for access and traversal out ofthe list object and put it into an iterator
object. The Iterator class defines an interface for accessing the list's elements. An
iterator object is responsible for keeping track of the current element; that is, it
knows which elements have been traversed already.

For example, a List class would call for a Listlterator with the following relationship between them:

Before you can instantiate Listlterator, you must supply the List to traverse. Once
you have the Listlterator instance, you can accessthe list's elements sequentially.
The Currentltem operation returns the current element in the list, First initializes
the current element to the first element, Next advances the current element to
the next element, and IsDone tests whether we've advanced beyond the last
element—that is, we're finished with the traversal.

Separating the traversal mechanism from the List object lets us define iterators
for different traversal policies without enumerating them in the List interface. For
example, FilteringListlterator might provide access only to those elements that
satisfy specific filtering constraints.

Notice that the iterator and the list are coupled, and the client must know that
it is a list that's traversed as opposed to some other aggregate structure. Hence
the client commits to a particular aggregate structure. It would be better if we
could change the aggregate class without changing client code. Wecan do this by
generalizing the iterator concept to support polymorphic iteration.

As an example, let's assume that we also have a SkipList implementation of a
list. A skiplist [Pug90] is a probabilistic data structure with characteristics similar
to balanced trees. Wewant to be able to write code that works for both List and
SkipList objects.

We define an AbstractListclassthat provides a common interface for manipulating
lists. Similarly, we need an abstract Iterator class that defines a common iteration
interface. Then we can define concrete Iterator subclasses for the different list
implementations. As a result, the iteration mechanism becomes independent of
concrete aggregate classes.

The remaining problem is how to create the iterator. Since we want to write code
that's independent of the concrete List subclasses, we cannot simply instantiate
a specific class. Instead, we make the list objects responsible for creating their
corresponding iterator. This requires an operation like Createlterator through
which clients request an iterator object.

Createlterator is an example of a factory method (see Factory Method (107)).We
use it here to let a client ask a list object for the appropriate iterator. The Factory
Method approach give rise to two class hierarchies, one for lists and another for
iterators. The Createlterator factory method "connects" the two hierarchies.

---

Use the Iterator pattern when your collection has a complex data structure under the hood, but you want to hide its
complexity from clients (either for convenience or security reasons).

The iterator encapsulates the details of working with a complex data structure, providing the client with several simple
methods of accessing the collection elements. While this approach is very convenient for the client, it also protects
the collection from careless or malicious actions which the client would be able to perform if working with the
collection directly.

Use the pattern to reduce duplication of the traversal code across your app.

The code of non-trivial iteration algorithms tends to be very bulky. When placed within the business logic of an app, it
may blur the responsibility of the original code and make it less maintainable. Moving the traversal code to designated
iterators can help you make the code of the application more lean and clean.

Use the Iterator when you want your code to be able to traverse different data structures or when types of these
structures are unknown beforehand.

The pattern provides a couple of generic interfaces for both collections and iterators. Given that your code now uses
these interfaces, it’ll still work if you pass it various kinds of collections and iterators that implement these
interfaces.

### Known Uses

- ...

Data Structure Traversal: Iterators are commonly used to traverse collections such as arrays, lists, trees, and graphs
without exposing their internal structure. For instance, looping through elements of a list without knowing the specific
implementation of the list.

Aggregate Operations: In programming languages that support functional programming paradigms (like Java 8+ with
streams), iterators are used implicitly behind the scenes to perform aggregate operations like map, filter, reduce,
etc., on collections.

Database Query Results: When fetching data from a database, iterators can be employed to go through query results row by
row, abstracting the database access details.

File System Navigation: Iterators can be utilized to traverse file system directories and access files or folders in a
structured manner without revealing the file system implementation details.

Menu Navigation: In graphical user interfaces, iterators can be used to navigate through menus or lists of items,
allowing users to interact with elements sequentially.

Parsing and Tokenization: In parsing tasks, iterators can be employed to traverse tokens or elements in a language
grammar, facilitating parsing operations without exposing the parsing algorithm details.

Caching and Lazy Loading: Iterators can be used for lazy loading or caching data, where elements are fetched only when
needed, improving performance and resource utilization.

Custom Collection Iteration: Implementing custom collections often involves creating iterators tailored to the specific
structure and traversal requirements of those collections.

Multithreading and Synchronization: In concurrent programming, iterators can be employed to safely access and traverse
shared data structures by providing thread-safe iteration mechanisms.

Network Protocol Handling: When dealing with network protocols that involve sequential handling of packets or messages,
iterators can facilitate traversal and processing of incoming data.

Graphical Drawing Applications: In applications that involve drawing shapes or graphical elements, iterators can be used
to iterate through a series of shapes or points to render them on a canvas or screen.

Event Handling: When handling events in a system (like GUI events or system events), iterators can assist in
sequentially processing events that occur over time.

Simulation and Modeling: Iterators can be utilized in simulation or modeling software to iterate through iterations or
time steps in a simulation, processing each step or event in the model.

Text Processing and Parsing: In text processing tasks such as tokenizing text, searching for patterns, or performing
lexical analysis, iterators can help traverse through characters, words, or tokens in a text document.

Audio/Video Processing: For applications involving audio or video processing, iterators can aid in sequential access to
frames, samples, or chunks of media data for processing or analysis.

Genetic Algorithms and Optimization: In optimization algorithms or genetic algorithms, iterators can be used to iterate
through possible solutions or generations in search of an optimal solution.

Undo/Redo Functionality: Iterators can assist in implementing undo/redo functionality by traversing through a history of
actions, allowing users to revert or redo changes sequentially.

Inventory Management Systems: When managing inventory in a system, iterators can help iterate through items, manage
stock, or perform operations on different categories of items.

Mathematical Computations: In mathematical computations involving matrices, vectors, or complex data structures,
iterators can facilitate traversal for various mathematical operations.

IoT Data Processing: In IoT (Internet of Things) applications, iterators can be used to process streams of sensor data,
iterating through sensor readings or events for analysis or actions.

All implementations of java.util.Iterator (thus among others also java.util.Scanner!).
All implementations of java.util.Enumeration

Iterators are common in object-oriented systems. Most collection class libraries
offer iterators in one form or another.
Here's an example from the Booch components [Boo94], a popular collection
class library. It provides both a fixed size (bounded) and dynamically growing
(unbounded) implementation of a queue. The queue interface is defined by an
abstract Queue class. To support polymorphic iteration over the different queue
implementations, the queue iterator is implemented in the terms of the abstract
Queue class interface. This variation has the advantage that you don't need a
factory method to ask the queue implementations for their appropriate iterator.
However, it requires the interface of the abstract Queue class to be powerful
enough to implement the iterator efficiently.
Iterators don't have to be defined as explicitly in Smalltalk. The standard collection
classes (Bag, Set, Dictionary, OrderedCollection, String, etc.) define an internal
iterator method do:, which takes a block (i.e., closure) as an argument. Each
element in the collection is bound to the local variable in the block; then the block
is executed. Smalltalk also includes a set of Stream classes that support an iterator-
like interface. ReadStream is essentially an Iterator, and it can act as an external
iterator for all the sequential collections. There are no standard external iterators
for nonsequential collections such as Set and Dictionary.
Polymorphic iterators and the cleanup Proxy described earlier are provided by
the ET++ container classes [WGM88]. The Unidraw graphical editing framework
classes use cursor-based iterators [VL90].
ObjectWindows 2.0 [Bor94] provides a class hierarchy of iterators for containers.
You can iterate over different container types in the same way. The ObjectWindow
iteration syntax relies on overloading the postincrement operator ++ to advance
the iteration.

### Categorization

Purpose:  **Behavioral**  
Scope:    **Object**   
Mechanisms: **Polymorphism**

Behavioral patterns are concerned with algorithms and the assignment of responsibilities between objects.
Behavioral patterns describe not just patterns of objects or classes
but also the patterns of communication between them.

Behavioral class patterns use inheritance to distribute behavior between classes. This chapter includes two such
patterns.

Behavioral object patterns use object composition rather than inheritance.
Some describe how a group of peer objects cooperate to perform a task that no single object can carry out by itself.
An important issue here is how peer objects know about each other.
Peers could maintain explicit references to each other, but that would increase their coupling.
Some patterns provide indirection to allow loose coupling
mediator, chain of responsibility, observer
Other behavioral object patterns are concerned with encapsulating behavior in an object and delegating requests to it.
strategy, command, state, visitor, iterator

### Aspects that can vary

- How an aggregate's elements are accessed, traversed.

### Solution to causes of redesign

- Algorithmic dependencies.
    - Algorithms are often extended, optimized, and replaced, forcing dependants to also change.

### Consequences

| Advantages                                         | Disadvantages                       |
|----------------------------------------------------|-------------------------------------|
| :heavy_check_mark: **Short description.** <br> ... | :x: **Short description.** <br> ... |

The Iterator pattern has three important consequences:

1. It supports variations in the traversal of an aggregate. Complex aggregates may
   be traversed in many ways. For example, code generation and semantic
   checking involve traversing parse trees. Code generation may traverse the
   parse tree inorder or preorder. Iterators make it easy to change the traversal
   algorithm:Justreplace the iterator instance with a different one. Youcan also
   define Iterator subclasses to support new traversals.
2. Iterators simplify the Aggregate interface. Iterator's traversal interface obviates
   the need for a similar interface in Aggregate, thereby simplifying the aggregate's interface.
3. More than one traversal can be pending on an aggregate. An iterator keeps track
   of its own traversal state. Therefore you can have more than one traversal in
   progress at once.

Single Responsibility Principle. You can clean up the client code and the collections by extracting bulky traversal
algorithms into separate classes.
Open/Closed Principle. You can implement new types of collections and iterators and pass them to existing code without
breaking anything.
You can iterate over the same collection in parallel because each iterator object contains its own iteration state.
For the same reason, you can delay an iteration and continue it when needed.

Applying the pattern can be an overkill if your app only works with simple collections.
Using an iterator may be less efficient than going through elements of some specialized collections directly.

### Relations with Other Patterns

- ...

Composite (163):Iterators are often applied to recursive structures such as Composites
Factory Method (107):Polymorphic iterators rely on factory methods to instantiate
the appropriate Iterator subclass.
Memento (283) is often used in conjunction with the Iterator pattern. An iterator
can use a memento to capture the state of an iteration. The iterator stores the
memento internally.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **The pattern can be implemented by ...**

Iterator can be implemented by the navigation methods (such as next, previous and others). Client code that uses
iterators might not have direct access to the collection being traversed.

(recognizable by behavioral methods sequentially returning instances of a different type from a queue)

### Structure

```mermaid
classDiagram
    class Aggregate {
        <<interface>>
        +createIterator(): Iterator
    }

    class ConcreteAggregate {
        -items: array
        +createIterator(): Iterator
    }

    class Iterator {
        <<interface>>
        +first()
        +next()
        +isDone(): boolean
        +currentItem()
    }

    class ConcreteIterator {
        -aggregate: ConcreteAggregate
        -current: integer
        +first()
        +next()
        +isDone(): boolean
        +currentItem()
    }

    Aggregate <|.. ConcreteAggregate: implements
    Iterator <|.. ConcreteIterator: implements
    Aggregate --> Iterator: instantiates
    ConcreteAggregate --o ConcreteIterator: aggregated by
```

### Variations

_Variation name:_

- **VariationA**: ...
    - :heavy_check_mark: ...
    - :x: ...
- **VariationB**: ...
    - :heavy_check_mark: ...
    - :x: ...

Iterator has many implementation variants and alternatives. Some important ones
follow. The trade-offs often depend on the control structures your language pro-
vides. Some languages (CLU [LG86], for example) even support this pattern di-
rectly.

1. Who controls the iteration? A fundamental issue is deciding which party con-
   trols the iteration, the iterator or the client that uses the iterator. When the
   client controls the iteration, the iterator is called an external iterator, and
   when the iterator controls it, the iterator is an internal iterator. 2 Clients that
   use an external iterator must advance the traversal and request the next el-
   ement explicitly from the iterator. In contrast, the client hands an internal
   iterator an operation to perform, and the iterator applies that operation to
   every element in the aggregate.
   External iterators are more flexible than internal iterators. It's easy to compare
   two collections for equality with an external iterator, for example, but it's
   practically impossible with internal iterators. Internal iterators are especially
   weak in a language like C++ that does not provide anonymous functions,
   closures, or continuations like Smalltalk and CLOS. But on the other hand, internal iterators are easier to use,
   because they define the iteration logic for
   you.
2. Who defines the traversal algorithm? The iterator is not the only place where the
   traversal algorithm can be defined. The aggregate might define the traversal
   algorithm and use the iterator to store just the state of the iteration. We call
   this kind of iterator a cursor, since it merely points to the current position in
   the aggregate. A client will invoke the Next operation on the aggregate with
   the cursor as an argument, and the Next operation will change the state of
   the cursor.3
   If the iterator is responsible for the traversal algorithm, then it's easy to use
   different iteration algorithms on the same aggregate, and it can also be easier
   to reuse the same algorithm on different aggregates. On the other hand,
   the traversal algorithm might need to access the private variables of the
   aggregate. If so, putting the traversal algorithm in the iterator violates the
   encapsulation of the aggregate.
3. How robust is the iterator? It can be dangerous to modify an aggregate while
   you're traversing it. If elements are added or deleted from the aggregate,
   you might end up accessing an element twice or missing it completely. A
   simple solution is to copy the aggregate and traverse the copy, but that's too
   expensive to do in general.
   A robust iterator ensures that insertions and removals won't interfere with
   traversal, and it does it without copying the aggregate. There are many ways
   to implement robust iterators. Most rely on registering the iterator with the
   aggregate. On insertion or removal, the aggregate either adjusts the internal
   state of iterators it has produced, or it maintains information internally to
   ensure proper traversal.
   Kofler provides a good discussion of how robust iterators are implemented
   in ET++ [Kof93]. Murray discusses the implementation of robust iterators
   for the USL StandardComponents' List class [Mur93].
4. Additional Iterator operations. The minimal interface to Iterator consists of
   the operations First, Next, IsDone, and Currentltem.4 Some additional op-
   erations might prove useful. For example, ordered aggregates can have a
   Previous operation that positions the iterator to the previous element. A
   SkipTo operation is useful for sorted or indexed collections. SkipTo positions
   the iterator to an object matching specific criteria.
5. Using polymorphic iterators in C++. Polymorphic iterators have their cost. They
   require the iterator object to be allocated dynamically by a factory method.
   Hence they should be used only when there's a need for polymorphism.
   Otherwise use concrete iterators, which can be allocated on the stack. Polymorphic iterators have another drawback:
   the client is responsible for
   deleting them. This is error-prone, because it's easy to forget to free a heap-
   allocated iterator object when you're finished with it. That's especially likely
   when there are multiple exit points in an operation. And if an exception is
   triggered, the iterator object will never be freed.
   The Proxy (207) pattern provides a remedy. We can use a stack-allocated
   proxy as a stand-in for the real iterator. The proxy deletes the iterator in
   its destructor. Thus when the proxy goes out of scope, the real iterator will
   get deallocated along with it. The proxy ensures proper cleanup, even in
   the face of exceptions. This is an application of the well-known C++ tech-
   nique "resource allocation is initialization" [ES90]. The Sample Code gives
   an example.
6. Iterators may have privileged access. An iterator can be viewed as an extension
   of the aggregate that created it. The iterator and the aggregate are tightly cou-
   pled. We can express this close relationship in C++ by making the iterator a
   friend of its aggregate. Then you don't need to define aggregate operations
   whose sole purpose is to let iterators implement traversal efficiently.
   However, such privileged access can make defining new traversals difficult,
   since it'll require changing the aggregate interface to add another friend.
   To avoid this problem, the Iterator class can include protected operations
   for accessing important but publicly unavailable members of the aggregate.
   Iterator subclasses (and only Iterator subclasses) may use these protected
   operations to gain privileged access to the aggregate.
7. Iterators for composites. External iterators can be difficult to implement over
   recursive aggregate structures like those in the Composite (163) pattern, be-
   cause a position in the structure may span many levels of nested aggregates.
   Therefore an external iterator has to store a path through the Composite to
   keep track of the current object. Sometimes it's easier just to use an internal
   iterator. It can record the current position simply by calling itself recursively,
   thereby storing the path implicitly in the call stack.
   If the nodes in a Composite have an interface for moving from a node to
   its siblings, parents, and children, then a cursor-based iterator may offer a
   better alternative. The cursor only needs to keep track of the current node; it
   can rely on the node interface to traverse the Composite.
   Composites often need to be traversed in more than one way. Preorder,
   postorder, inorder, and breadth-first traversals are common. You can support
   each kind of traversal with a different class of iterator.
8. Null iterators. A Nulllterator is a degenerate iterator that's helpful for han-
   dling boundary conditions. By definition, a Nulllterator is always done with
   traversal; that is, its IsDone operation always evaluates to true.
   Nulllterator can make traversing tree-structured aggregates (like Compos-
   ites) easier. At each point in the traversal, we ask the current element for
   an iterator for its children. Aggregate elements return a concrete iterator as usual. But leaf elements return an
   instance of Nulllterator. That lets us
   implement traversal over the entire structure in a uniform way

---

internal or external iterator

- We implemented an external iterator, which means that the client controls the iteration by calling next() to get the
  next element. An internal iterator is controlled by the iterator itself.
- Internal iterators are less flexible than external iterators because the client doesn’t have control of the iteration.
  However, some might argue that they are easier to use because you just hand them an operation and tell them to
  iterate,

### Implementation

In the example we apply the iterator pattern to a ... system.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Iterator](https://refactoring.guru/design-patterns/iterator)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)

<br>
<br>
